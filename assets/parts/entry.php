<?php

	// entry.php
	// description: entry format for archive, index, & search.
	// ----------------------------------------------------------------

?>

<section>
    <a href="<?php the_permalink() ?>">
        <?php the_post_thumbnail('full'); ?>
    </a>
    <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
        <?php the_title( '<h2>', '</h2>' ); ?>
    </a>
    <?php 
        include('byline.php');
        the_excerpt();
    ?>
</section>
