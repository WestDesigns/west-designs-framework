<?php

	// byline.php
	// description: byline for posts.
	// ----------------------------------------------------------------

?>

<p role="byline">
	Posted on <?php the_time('F j, Y'); ?> by <?php the_author_posts_link(); ?>
</p>

<hr>
