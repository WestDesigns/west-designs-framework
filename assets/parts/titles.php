<?php

	// title.php
	// description: if else statements for page titles.
	// ----------------------------------------------------------------

	if ( is_front_page() || is_404() ) {
        // do nothing
	}

    // elseif ( tribe_is_month() || tribe_is_upcoming() && !is_tax() ) { // the events calendar
	//     // 	echo "Events";
	// }

    // - // archive-example
        // elseif ( is_post_type_archive('example') ) {
        //     the_title();
        // } 

    elseif ( is_archive() ) {
		the_archive_title();
	}

    elseif ( is_author() ) {
		echo "Author: " . get_the_author();
	}

    elseif ( is_category() ) {
        the_archive_title();
    }

    elseif ( is_tag() ) {
        the_archive_title();
    }

    elseif ( is_home() ) {
		echo "Our Blog";
	}

    elseif ( is_search() ) {
        echo "Search: " . get_search_query();
    }

    // - // single-example
        // elseif ( is_singular('example') ) {
        //     the_title();
        // }

    elseif ( is_single() ) {
		the_title();
	}

    // - // page-example
        // elseif ( is_page(#) ) {
        //     the_title();
        // }

    elseif ( is_page() ) {
        the_title();
    }

    else {
		the_title();
    }

?>
