<?php

	// missing.php
	// description: loop for missing pages or posts.
	// ----------------------------------------------------------------

	if ( is_search() ) :
		?>
			<h2>Sorry, no results were found.</h2>
			<p>
				Try searching again with a different keyword, name, or date.
			</p>
		<?
		get_search_form();
	else :
		?>
			<h2>Sorry, nothing is here yet.</h2>
			<p>
				Add content on this page or post to be displayed.
			</p>
		<?
		// get_search_form();
	endif;

?>
