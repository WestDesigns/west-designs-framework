<?php

	// breadcrumbs.php
	// description: loads breadcrumbs.
	// ----------------------------------------------------------------

?>

<div role="breadcrumbs">
    <section>
        <?php
            if ( function_exists('yoast_breadcrumb') )
                {yoast_breadcrumb('<p>','</p>');
            } else {
                // do nothing
            }
        ?>
    </section>
</div>
