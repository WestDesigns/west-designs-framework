<?php

	// featured.php
	// description: loads the page title & featured image.
	// ----------------------------------------------------------------

    // get featured images
    $featured = wp_get_attachment_image_src ( get_post_thumbnail_id(), 'full');

    // adaptive-backgrounds
    // data-adaptive-background data-ab-css-background

    // front-page & 404
	if ( is_front_page() || is_404() ) {
		// do nothing
	}

	//- // archive-example
		elseif ( is_post_type_archive('example') ) {
			?>
				<div role="featured" class="example">
					<section>
                        <h1><?php include_once('titles.php'); ?></h1>
					</section>
				</div>
			<?
		}

	// archive & author
	elseif ( is_archive() || is_author() ) {
		?>
			<div role="featured" class="archive">
				<section>
                    <h1><?php include_once('titles.php'); ?></h1>
				</section>
			</div>
		<?
	}

	// category & tag
	elseif ( is_category() || is_tag() ) {
		?>
			<div role="featured" class="category">
				<section>
                    <h1><?php include_once('titles.php'); ?></h1>
				</section>
			</div>
		<?
	}

	// index
	elseif ( is_home() ) {
		?>
			<div role="featured" class="index">
				<section>
                    <h1><?php include_once('titles.php'); ?></h1>
				</section>
			</div>
		<?
	}

	// search
	elseif ( is_search() ) {
		?>
			<div role="featured" class="search">
				<section>
                    <h1><?php include_once('titles.php'); ?></h1>
				</section>
			</div>
		<?
	}

	//- // single-example
		elseif ( is_singular('example') ) {
			?>
                <div role="featured" class="example">
                    <section>
                        <h1><?php include_once('titles.php'); ?></h1>
                    </section>
                </div>
			<?
		}

	// single
	elseif ( is_single() ) {
		?>
            <div role="featured" style="<?php if ( has_post_thumbnail() ) { echo "background: url('$featured[0]') center center no-repeat; background-size: cover;"; } else {}; ?>">
                <section>
                    <h1><?php include_once('titles.php'); ?></h1>
                </section>
            </div>
		<?
	}

	//- // is_page(#)
		// elseif ( is_page(#) ) {
		// 	// do nothing
		// }

	// page
	elseif ( is_page() ) {
		?>
            <div role="featured" style="<?php if ( has_post_thumbnail() ) { echo "background: url('$featured[0]') center center no-repeat; background-size: cover;" ; } else {}; ?>">
                <section>
                    <h1><?php include_once('titles.php'); ?></h1>
                </section>
            </div>
		<?
	}

	// rest of the website
	else {
		?>
            <div role="featured" style="<?php if ( has_post_thumbnail() ) { echo "background: url('$featured[0]') center center no-repeat; background-size: cover;"; } else {}; ?>">
                <section>
                    <h1><?php include_once('titles.php'); ?></h1>
                </section>
            </div>
		<?
	}

?>
