// app.js
// description: theme javascript
// ----------------------------------------------------------------

// foundation
jQuery(document).foundation();

// functions
jQuery(document).ready(function($) {

    // slick-slider
    $('').slick({
    	dots: false,
    	arrows: false,
    	infinite: true,
    	autoplay: true,
    	autoplaySpeed: 6000,
    	speed: 800
    });

    // scroll-reveal
    window.sr = ScrollReveal();
    // sr.reveal('.class');

    // parallax
    // .parallax (xPosition, speedFactor, outerHeight) options:
    // xPosition - horizontal position of the element
    // inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling
    // outerHeight (true/false) - whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
    // background: url(images/trainers.png) 50% 0 no-repeat fixed;
    // http://ianlunn.co.uk/articles/recreate-nikebetterworld-parallax/
    $('.parallax').parallax("50%", 0.1);

    // menu animation
    $('.is-dropdown-submenu-parent').hover(function() {
    	$(this).children('ul').stop(true, false, true).slideToggle(150);
    });

    // fix menu flicker
    $('nav ul').animate({opacity: 1}, 1000);

    // remove empty p tags created by wordpress inside of accordion and orbit
    $('.accordion p:empty, .orbit p:empty').remove();

    // makes sure last grid item floats left
    $( '.archive-grid .columns' ).last().addClass( 'end' );

    // adds flex video to youtube and vimeo embeds
    $( 'iframe[src*="youtube.com"], iframe[src*="vimeo.com"]' ).wrap("<div class='flex-video'/>");

});
