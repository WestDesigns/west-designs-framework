<?php

	// index.php
	// description: page for the main post loop.
	// ----------------------------------------------------------------

get_header(); ?>

<div role="index" class="global">
    <main>
        <article>
            <?php // archive loop
				if ( have_posts() ) : while ( have_posts() ) : the_post();
                    include('assets/parts/entry.php');
				endwhile;
					wdf_pagination();
				else :
                    include_once('assets/parts/missing.php');
				endif;
			?>
        </article>
        <aside>
            <?php get_sidebar(); ?>
        </aside>
    </main>
</div>

<?php get_footer(); ?>
