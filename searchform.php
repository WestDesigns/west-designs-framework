<?php

	// searchform.php
	// description: page for the search form.
	// ----------------------------------------------------------------

?>

<form class="searchform" method="get" action="<?php echo home_url( '/' ); ?>">
	<label>
		<span><?php echo _x( 'Search for:', 'label', 'wdf' ) ?></span>
		<input type="search" placeholder="<?php echo esc_attr_x( 'Search...', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'wdf' ) ?>" />
	</label>
	<input type="submit" value="<?php echo esc_attr_x( 'Search', 'wdf' ) ?>" />
</form>
