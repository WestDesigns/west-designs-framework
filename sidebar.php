<?php

	// sidebar.php
	// description: page for the sidebar.
	// ----------------------------------------------------------------

    // if else
    if ( ! is_page('example') ) {
        dynamic_sidebar('primary');
    } else {
        dynamic_sidebar('secondary');
    }


?>
