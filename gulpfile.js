// gulpfile.js
// description: node for parsing images, scss and js.
// ----------------------------------------------------------------

// load packages
var gulp 		= require('gulp'),
	sass 		= require('gulp-ruby-sass'),
	del 		= require('del'),
	prefixer 	= require('gulp-autoprefixer'),
	concat 		= require('gulp-concat'),
	minify 		= require('gulp-clean-css'),
	rename 		= require('gulp-rename'),
	stylish		= require('jshint-stylish'),
	jshint 		= require('gulp-jshint'),
	uglify		= require('gulp-uglify'),
	cache		= require('gulp-cache'),
	imagemin	= require('gulp-imagemin'),
	plumber		= require('gulp-plumber'),
	gutil 		= require('gulp-util'),
	notify		= require('gulp-notify')
	favicon 	= require ('gulp-real-favicon');
	fs 			= require('fs');
    FAVICON_DATA_FILE = 'faviconData.json';

    // delete
gulp.task('clean:dist', function () {
	return del(['assets/dist/*']);
});

// scss
gulp.task('scss', function() {
	return sass(['assets/scss/_settings.scss', 'assets/scss/*.scss'], { style: 'compressed' })
	.pipe(plumber(function(error) {
        gutil.log(gutil.colors.red(error.message));
        this.emit('end');
    }))
	.pipe(prefixer({
		browsers: ['last 2 versions'],
		cascade: false
	}))
	.pipe(concat('app.min.css'))
	.pipe(minify())
	.pipe(gulp.dest('assets/dist'))
	.pipe(notify({ message: 'scss processed.' }));
});

// js
gulp.task('js', function() {
	return gulp.src('assets/js/*.js')
	.pipe(concat('app.js'))
	.pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('jshint-stylish'))
	.pipe(uglify({
		mangle: true,
		preserveComments: false
	}))
	.pipe(rename({ suffix: '.min' }))
	.pipe(gulp.dest('assets/dist'))
	.pipe(notify({ message: 'js processed.' }));
});


// favicons
gulp.task('favicons', function(done) {
	favicon.generateFavicon({
		masterPicture: 'favicon.png',
		dest: 'assets/favicons/',
		iconsPath: '/',
		design: {
			ios: {
				pictureAspect: 'noChange',
				assets: {
					ios6AndPriorIcons: false,
					ios7AndLaterIcons: false,
					precomposedIcons: false,
					declareOnlyDefaultIcon: true
				},
				appName: 'Outdoors360'
			},
			desktopBrowser: {},
			windows: {
				pictureAspect: 'noChange',
				backgroundColor: '#da532c',
				onConflict: 'override',
				assets: {
					windows80Ie10Tile: false,
					windows10Ie11EdgeTiles: {
						small: false,
						medium: true,
						big: false,
						rectangle: false
					}
				},
				appName: 'Outdoors360'
			},
			androidChrome: {
				pictureAspect: 'noChange',
				themeColor: '#ffffff',
				manifest: {
					name: 'Outdoors360',
					display: 'standalone',
					orientation: 'notSet',
					onConflict: 'override',
					declared: true
				},
				assets: {
					legacyIcon: false,
					lowResolutionIcons: false
				}
			},
			safariPinnedTab: {
				pictureAspect: 'blackAndWhite',
				threshold: 75,
				themeColor: '#5bbad5'
			}
		},
		settings: {
			scalingAlgorithm: 'Mitchell',
			errorOnImageTooSmall: false
		},
		markupFile: FAVICON_DATA_FILE
	}, function() {
		done();
	});
});

gulp.task('check-for-favicon-update', function(done) {
	var currentVersion = JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).version;
	favicon.checkForUpdates(currentVersion, function(err) {
		if (err) {
			throw err;
		}
	});
});

// default
gulp.task('default', function() {
	// delete dist files
	gulp.task('clean:dist');
	// watch .scss files
	gulp.watch('assets/scss/*.scss', ['scss']);
	// watch .js files
	gulp.watch('assets/js/*.js', ['js']);
});
