<!doctype html>
<html lang="en" class="tr-coretext tr-aa-subpixel">
<head>

    <!-- meta -->
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php // TODO: place description here ?>">
    <meta name="keywords" content="<?php // TODO: place keywords ?>">
    <meta name="author" content="West Designs">
    <meta name="contact" content="brandon@designsbywest.com & alex@designsbywest.com">
    <meta name="copyright" content="Copyright &copy; <?php echo date('Y'); ?> Outdoors360">
    <meta name="robots" content="<?php // TODO: place index,follow ?>">
    <meta http-equiv="content-type" content="text/html; charset=US-ASCII">
    <meta http-equiv="content-language" content="en-us">
    <meta name="resource-type" content="document">

    <!-- title -->
    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <!-- favicons -->
    <link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_directory'); ?>/assets/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/assets/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/assets/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_directory'); ?>/favicon.png">
    <link rel="manifest" href="<?php bloginfo('template_directory'); ?>/assets/favicons/manifest.json">
    <link rel="mask-icon" href="<?php bloginfo('template_directory'); ?>/assets/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="apple-mobile-web-app-title" content="Outdoors360">
    <meta name="application-name" content="Outdoors360">
    <meta name="theme-color" content="#ffffff">

    <!-- links -->
    <link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- wp_head -->
	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

    <div role="masthead">
        <section>
            <nav>
                <a href="#" class="button">Button</a>
                <ul class="social">
                    <li>
                        <a href="#">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <i class="fa fa-youtube"></i>
                        </a>
                    </li>
                </ul>
				<?php wdf_navigation_masthead(); ?>
			</nav>
        </section>
    </div>

    <header>
        <section>
            <div>
                <a href="<?php echo esc_url( home_url() ); ?>">
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/logo.png" alt="<?php bloginfo('name'); ?> Logo">
				</a>
            </div>
            <nav>
                <?php wdf_navigation_header(); ?>
            </nav>
        </section>
    </header>

    <?php
        // featured & breadcrumbs
        if ( ! is_front_page() || ! is_404() ) :
            include_once('assets/parts/featured.php');
            include_once('assets/parts/breadcrumbs.php');
        endif;
    ?>
