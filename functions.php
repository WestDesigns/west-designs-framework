<?php

	// functions.php
	// description: default theme functions.
	// ----------------------------------------------------------------

    // wordpress defaults
    if ( is_user_logged_in() ) { show_admin_bar(true); }
    if ( ! isset( $content_width ) ) { $content_width = 1100; }

    function wdf_theme_supports() {
		// add wp thumbnail support
		add_theme_support( 'post-thumbnails' );
		// default thumbnail size
		set_post_thumbnail_size(125, 125, true);
		// add rss support
		add_theme_support( 'automatic-feed-links' );
		// add support for wp controlled title tag
		add_theme_support( 'title-tag' );
		// add html5 support
		add_theme_support( 'html5',
		    array(
		    	'comment-list',
		    	'comment-form',
		    	'search-form',
		    )
		);
	}

    // enable iframe import
    add_filter('force_filtered_html_on_import' , '__return_false');

    // disable imgset
    remove_filter('the_content', 'wp_make_content_images_responsive');

    // require_once
	// require_once(get_template_directory() . '/assets/parts/');

    // fonts
    function wdf_fonts() {
        if ( ! is_admin() ) {

            // font-family: 'Montserrat', sans-serif;
            // font-family: 'Roboto', sans-serif;
            // <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
            wp_enqueue_style('fonts', 'https://fonts.googleapis.com/css?family=Montserrat:400,700|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i', array(), '', 'all');

        }
    }

    add_action('wp_enqueue_scripts', 'wdf_fonts');

    // enqueue
    function wdf_enqueue() {
        if ( ! is_admin() ) {

            global $wp_styles;
            wp_enqueue_script('jquery');

            wp_enqueue_script('theme', 'https://cdn.jsdelivr.net/g/foundation@6.2.3,jquery.slick@1.6.0,jquery.parallax@1.1.3,scrollreveal.js@3.3.1', array('jquery'), '', true);
            wp_enqueue_script('motion',  get_template_directory_uri() . '/bower_components/motion-ui/dist/motion-ui.min.js', array('jquery'), '', true);

            wp_enqueue_style('fontawesome', 'https://cdn.jsdelivr.net/fontawesome/4.6.3/css/font-awesome.min.css', array(), '', 'all');
            wp_enqueue_style('slick', 'https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css', array(), '', 'all');

            wp_enqueue_script('app',  get_template_directory_uri() . '/assets/dist/app.min.js', array('jquery'), '', true);
            wp_enqueue_style('app', get_template_directory_uri() . '/assets/dist/app.min.css', array(), '', 'all');

            wp_enqueue_style('style', get_template_directory_uri() . '/style.css', array(), '', 'all');

            // comment replies
			if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
				wp_enqueue_script('comment-reply');
			}

        }
    }

    add_action('init', 'wdf_enqueue' , 999);

    // footer
	function wdf_footer() {
		_e('<span id="footer-thankyou">Developed by <a href="www.designsbywest.com" target="_blank">West Designs</a></span>.', 'wdf');
	}

	add_filter('admin_footer_text', 'wdf_footer');

    // wdf-menus.php
	// description: enables wordpress menus.
	// ----------------------------------------------------------------

    // register menus
    register_nav_menus(
		array(
			// wdf_navigation_masthead();
			'navigation-masthead' => __( 'Masthead Menu', 'wdf' ),
			// wdf_navigation_header();
			'navigation-header' => __( 'Header Menu', 'wdf' ),
			// wdf_navigation_footer();
			'navigation-footer' => __( 'Footer Menu', 'wdf' )
		)
	);

    // masthead
	function wdf_navigation_masthead() {
		$args = array(
			'theme_location' => 'navigation-masthead',
			'menu' => '',
			'container' => false,
			'container_class' => '',
			'container_id' => '',
			'menu_class' => 'menu',
			'menu_id' => '',
			'echo' => true,
			'fallback_cb' => false,
			'before' => '',
			'after' => '',
			'link_before' => '',
			'link_after' => '',
			'items_wrap' => '<ul>%3$s</ul>',
			'depth' => 5,
			'walker' => new Topbar_Menu_Walker()
		);
		wp_nav_menu($args);
	}

	// header
	function wdf_navigation_header() {
		$args = array(
			'theme_location' => 'navigation-header',
			'menu' => '',
			'container' => false,
			'container_class' => '',
			'container_id' => '',
			'menu_class' => 'menu',
			'menu_id' => '',
			'echo' => true,
			'fallback_cb' => false,
			'before' => '',
			'after' => '',
			'link_before' => '',
			'link_after' => '',
			'items_wrap' => '<ul class="%2$s" style="opacity: 0;" data-responsive-menu="accordion medium-dropdown">%3$s</ul>',
			'depth' => 5,
			'walker' => new Topbar_Menu_Walker()
		);
		wp_nav_menu($args);
	}

	// footer
	function wdf_navigation_footer() {
		$args = array(
			'theme_location' => 'navigation-footer',
			'menu' => __( 'Footer Menu', 'wdf' ),
			'container' => false,
			'menu_class' => 'menu',
			'fallback_cb' => false,
			'depth' => 0,
		);
		wp_nav_menu( $args );
	}

    // menu-walker, pagination, & related
    // ----------------------------------------------------------------

    // walker
    class Topbar_Menu_Walker extends Walker_Nav_Menu {
	    function start_lvl(&$output, $depth = 0, $args = Array() ) {
	        $indent = str_repeat("\t", $depth);
	        $output .= "\n$indent<ul class=\"menu\">\n";
	    }
	}

    // pagination
	function wdf_pagination($before = '', $after = '') {
		global $wpdb, $wp_query;
		$request = $wp_query->request;
		$posts_per_page = intval(get_query_var('posts_per_page'));
		$paged = intval(get_query_var('paged'));
		$numposts = $wp_query->found_posts;
		$max_page = $wp_query->max_num_pages;
		if ( $numposts <= $posts_per_page ) { return; }
		if(empty($paged) || $paged == 0) {
			$paged = 1;
		}
		$pages_to_show = 7;
		$pages_to_show_minus_1 = $pages_to_show-1;
		$half_page_start = floor($pages_to_show_minus_1/2);
		$half_page_end = ceil($pages_to_show_minus_1/2);
		$start_page = $paged - $half_page_start;
		if($start_page <= 0) {
			$start_page = 1;
		}
		$end_page = $paged + $half_page_end;
		if(($end_page - $start_page) != $pages_to_show_minus_1) {
			$end_page = $start_page + $pages_to_show_minus_1;
		}
		if($end_page > $max_page) {
			$start_page = $max_page - $pages_to_show_minus_1;
			$end_page = $max_page;
		}
		if($start_page <= 0) {
			$start_page = 1;
		}
		echo $before.'<div class="pagination"><ul class="menu">'."";
		if ($start_page >= 2 && $pages_to_show < $max_page) {
			$first_page_text = __( "First", 'wdf' );
			echo '<li><a href="'.get_pagenum_link().'" title="'.$first_page_text.'">'.$first_page_text.'</a></li>';
		}
		echo '<li>';
		previous_posts_link('Previous');
		echo '</li>';
		for($i = $start_page; $i  <= $end_page; $i++) {
			if($i == $paged) {
				echo '<li class="active"><a href="'.get_pagenum_link($i).'"> '.$i.' </a></li>';
			} else {
				echo '<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
			}
		}
		echo '<li>';
		next_posts_link('Next');
		echo '</li>';
		if ($end_page < $max_page) {
			$last_page_text = __( "Last", 'wdf' );
			echo '<li><a href="'.get_pagenum_link($max_page).'" title="'.$last_page_text.'">'.$last_page_text.'</a></li>';
		}
		echo '</ul></div>'.$after."";
	}

    // related posts
	function wdf_related() {
		global $post;
		$tags = wp_get_post_tags( $post->ID );
		if($tags) {
			foreach( $tags as $tag ) {
				$tag_arr .= $tag->slug . ',';
			}
			$args = array(
				'tag' => $tag_arr,
				'numberposts' => 3,
				'post__not_in' => array($post->ID)
			);
			$related_posts = get_posts( $args );
			if($related_posts) {
			echo '<h4>Related Posts</h4>';
			echo '<ul class="related">';
				foreach ( $related_posts as $post ) : setup_postdata( $post ); ?>
					<li>
						<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
						<?php get_template_part( 'assets/parts/content', 'byline' ); ?>
					</li>
				<?php endforeach; }
				}
		wp_reset_postdata();
		echo '</ul>';
	}

    // wdf-widgets.php
    // description: enables wordpress sidebar & widgets.
    // ----------------------------------------------------------------

    // default sidebars
	function wdf_sidebars() {

		// primary
		register_sidebar(array(
			'id' => 'primary',
			'name' => __('Primary Sidebar', 'wdf'),
			'description' => __('The primary sidebar.', 'wdf'),
			'class' => 'primary',
			'before_widget' => '<div class="sidebar primary">',
			'after_widget'  => '</div>',
			'before_title' => '<h4>',
			'after_title' => '</h4>',
		));

		// secondary
		register_sidebar(array(
			'id' => 'secondary',
			'name' => __('Secondary Sidebar', 'wdf'),
			'description' => __('The secondary sidebar.', 'wdf'),
			'class' => 'primary',
			'before_widget' => '<div class="sidebar secondary">',
			'after_widget'  => '</div>',
			'before_title' => '<h4>',
			'after_title' => '</h4>',
		));

		// tertiary
		register_sidebar(array(
			'id' => 'tertiary',
			'name' => __('Tertiary Sidebar', 'wdf'),
			'description' => __('The tertiary sidebar.', 'wdf'),
			'class' => 'primary',
			'before_widget' => '<div class="sidebar tertiary">',
			'after_widget'  => '</div>',
			'before_title' => '<h4>',
			'after_title' => '</h4>',
		));

        // widgets
        // ----------------------------------------------------------------

		// left widget
		register_sidebar( array(
			'id'            => 'left-widget',
			'name'          => __( 'Left Footer', 'wdf' ),
			'description'   => 'Left Widget.',
			'class'			=> 'left-widget',
			'before_widget' => '<div class="widget left">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4>',
			'after_title'   => '</h4>',
		) );

		// center widget
		register_sidebar( array(
			'id'            => 'center-widget',
			'name'          => __( 'Center Footer', 'wdf' ),
			'description'   => 'Center Widget.',
			'class'			=> 'center-widget',
			'before_widget' => '<div class="widget center">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4>',
			'after_title'   => '</h4>',
		) );

		// right widget
		register_sidebar( array(
			'id'            => 'right-widget',
			'name'          => __( 'Right Footer', 'wdf' ),
			'description'   => 'Right Widget.',
			'class'			=> 'right-widget',
			'before_widget' => '<div class="widget right">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4>',
			'after_title'   => '</h4>',
		) );

	}

	add_action( 'widgets_init', 'wdf_sidebars' );

    // wdf-clean.php
	// description: secures default wordpress.
	// ----------------------------------------------------------------

	// let's begin, shall we?
	// --------------------------------
	function wdf_clean() {

		// add theme support
		wdf_theme_supports();

		// clean wordpress images
	    add_filter( 'the_content', 'imgtag_fix' );
	    // clean login errors
	    add_filter( 'login_errors', 'login_errors_fix' );
	    // clean excerpt
		add_filter('excerpt_more', 'wdf_excerpt_more');
		// clean gallery output in wp
		add_filter('gallery_style', 'wdf_gallery_style');
	    // clean image unautop
	    add_filter( 'the_content', 'wdf_img_unautop', 30 );
	    // clean image editor
	    add_filter( 'get_image_tag', 'wdf_image_editor', 0, 4 );
	    // clean comment styles in wp_head()
		add_action('wp_head', 'wdf_remove_recent_comments_style', 1);
	    // clean image tag class
	    add_filter('get_image_tag_class', 'wdf_image_tag_class', 0, 4 );
	    // clean post captions
	    add_filter( 'img_caption_shortcode', 'wdf_cleaner_caption', 10, 3 );
	    // remove injected css for recent comments widget
		add_filter( 'wp_head', 'wdf_remove_wp_widget_recent_comments_style', 1 );
		// clean & sharpen images
	    add_filter( 'image_make_intermediate_size', 'ajx_sharpen_resized_files', 900 );

	    // ----------------------------------------------------------------

	    // remove EditURI link
		remove_action( 'wp_head', 'rsd_link' );
		// remove WP version
		remove_action( 'wp_head', 'wp_generator' );
		// remove post and comment feeds
		remove_action( 'wp_head', 'feed_links', 2 );
		// remove index link
		remove_action( 'wp_head', 'index_rel_link' );
		// remove windows live writer
		remove_action( 'wp_head', 'wlwmanifest_link' );
		// remove category feeds
		remove_action( 'wp_head', 'feed_links_extra', 3 );
		// adding sidebars to Wordpress
		add_action( 'widgets_init', 'wdf_sidebars' );
		// remove start link
		remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
		// remove previous link
		remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
		// remove links for adjacent posts
		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	  	// remove wordpress version from css
	  	add_filter( 'style_loader_src', 'wdf_remove_wp_ver_css_js', 9999 );

	  	// ----------------------------------------------------------------

	  	// edit wordpress login detail
	    add_filter('gettext', 'login_welcome', 10, 3);
		// add excerpt read more
	    add_filter( 'exerpt_more', 'excerpt_readmore' );
	    // edit single post redirect
	    add_action( 'template_redirect', 'single_result' );
	    // remove rss version
	    add_filter( 'the_generator', 'wdf_rss_version' );
	    // add html tags to editor
	    add_filter( 'tiny_mce_before_init', 'fb_change_mce_options' );
	    // edit search for permalinks
	    add_filter( 'template_redirect', 'change_search_url_rewrite' );
		// add ie conditional wrapper
	    add_filter( 'style_loader_tag', 'wdf_ie_conditional', 10, 2 );
	    // add subdomain redirects
	    add_filter( 'allowed_redirect_hosts', 'allowing_redirect_hosts' );
	    // edit visual editor as default
	    add_filter( 'wp_default_editor', create_function('', 'return "tinymce";') );

	}

	// clean wordpress images
	if( ! function_exists( 'imgtag_fix' ) ) {
	    function imgtag_fix( $content ) {
	        return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	    }
	}

	// clean login errors
	if( ! function_exists( 'login_errors_fix' ) ) {
	    function login_errors_fix( $error ) {
	        $error = '';
	        return $error;
	    }
	}

	// this removes the annoying […] to a Read More link
	if( ! function_exists( 'wdf_excerpt_more' ) ) {
		function wdf_excerpt_more($more) {
			global $post;
            return '... <a class="button more" href="'. get_permalink($post->ID) .'" title="'. __('Read', 'wdf') . get_the_title($post->ID).'">'. __('Read More', 'wdf') .'</a>';
		}
	}

	// remove injected CSS from gallery
	if( ! function_exists( 'wdf_gallery_style' ) ) {
		function wdf_gallery_style($css) {
			return preg_replace("!<style type='text/css'>(.*?)</style>!s", '', $css);
		}
	}

	// clean image unautop
	if( ! function_exists( 'wdf_img_unautop ' ) ) {
		function wdf_img_unautop($args) {
		    $args = preg_replace('/<p>\\s*?(<a .*?><img.*?><\\/a>|<img.*?>)?\\s*<\\/p>/s', '<figure>$1</figure>', $args);
		    return $args;
		}
	}

	// clean image editor
	if( ! function_exists( 'wdf_image_editor ' ) ) {
		function wdf_image_editor( $html, $id, $alt, $title ) {
			return preg_replace(array(
					'/\s+width="\d+"/i',
					'/\s+height="\d+"/i',
					'/alt=""/i'
				),
				array(
					'',
					'',
					'',
					'alt="' . $title . '"'
				),
				$html
			);
		}
	}

	// remove injected CSS from recent comments widget
	if( ! function_exists( 'wdf_remove_recent_comments_style' ) ) {
		function wdf_remove_recent_comments_style() {
			global $wp_widget_factory;
			if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
				remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
		  	}
		}
	}

	// clean image tag class
	if( ! function_exists( 'wdf_image_tag_class' ) ) {
		function wdf_image_tag_class($class, $id, $align, $size) {
			$align = 'align' . esc_attr($align);
			return $align;
		}
	}

	// clean post captions
	if( ! function_exists( 'wdf_cleaner_caption ' ) ) {
		function wdf_cleaner_caption( $output, $attr, $content ) {
			// we're not worried abut captions in feeds, so just return the output here
			if ( is_feed() )
				return $output;
			// set up the default arguments
			$defaults = array(
				'id' => '',
				'align' => 'alignnone',
				'width' => '',
				'caption' => ''
			);
			// merge the defaults with user input
			$attr = shortcode_atts( $defaults, $attr );
			// if the width is less than 1 or there is no caption, return the content wrapped between the [caption]< tags
			if ( 1 > $attr['width'] || empty( $attr['caption'] ) )
				return $content;
			// set up the attributes for the caption <div>
			$attributes = ' class="figure ' . esc_attr( $attr['align'] ) . '"';
			// open the caption <div>
			$output = '<figure' . $attributes .'>';
			// allow shortcodes for the content the caption was created for
			$output .= do_shortcode( $content );
			// append the caption text
			$output .= '<figcaption>' . $attr['caption'] . '</figcaption>';
			// close the caption </div>
			$output .= '</figure>';
			// return the formatted, clean caption
			return $output;
		}
	}

	// remove injected CSS for recent comments widget
	if( ! function_exists( 'wdf_remove_wp_widget_recent_comments_style' ) ) {
		function wdf_remove_wp_widget_recent_comments_style() {
			if ( has_filter('wp_head', 'wp_widget_recent_comments_style') ) {
				remove_filter('wp_head', 'wp_widget_recent_comments_style' );
			}
		}
	}

	// clean & sharpen images
	if( ! function_exists( 'ajx_sharpen_resized_files' ) ) {
	    function ajx_sharpen_resized_files( $resized_file ) {
	        $image = wp_load_image( $resized_file );
	        if ( !is_resource( $image ) )
	            return new WP_Error( 'error_loading_image', $image, $file );
	        $size = @getimagesize( $resized_file );
	        if ( !$size )
	            return new WP_Error('invalid_image', __('Could not read image size'), $file);
	        list($orig_w, $orig_h, $orig_type) = $size;
	        switch ( $orig_type ) {
	            case IMAGETYPE_JPEG:
	                $matrix = array(
	                    array(-1, -1, -1),
	                    array(-1, 16, -1),
	                    array(-1, -1, -1),
	                );
	                $divisor = array_sum(array_map('array_sum', $matrix));
	                $offset = 0;
	                imageconvolution($image, $matrix, $divisor, $offset);
	                imagejpeg($image, $resized_file,apply_filters( 'jpeg_quality', 90, 'edit_image' ));
	                break;
	            case IMAGETYPE_PNG:
	                return $resized_file;
	            case IMAGETYPE_GIF:
	                return $resized_file;
	        }
	        return $resized_file;
	    }
	}

	// ----------------------------------------------------------------

	// remove wordpress version from css
	if( ! function_exists( 'wdf_remove_wp_ver_css_js' ) ) {
		function wdf_remove_wp_ver_css_js( $src ) {
		    if( strpos( $src, '?ver=' ) ) {
		        $src = remove_query_arg( 'ver', $src );
		    }
		    return $src;
		}
	}

	// ----------------------------------------------------------------

	// edit wordpress login detail
	if( ! function_exists( 'login_welcome' ) ) {
		function login_welcome($translated, $text, $domain) {
		    if (!is_admin() || 'default' != $domain)
		        return $translated;
		    if (false !== strpos($translated, 'Howdy'))
		        return str_replace('Howdy', 'Welcome', $translated);
		    return $translated;
		}
	}

	// add read more
	if( ! function_exists( 'excerpt_readmore' ) ) {
		function excerpt_readmore($more) {
	    	return '... <a href="'. get_permalink($post->ID) . '" class="readmore">' . 'Read More' . '</a>';
		}
	}

	// edit single post redirect
	if( ! function_exists( 'single_result' ) ) {
		function single_result() {
		    if (is_search()) {
		        global $wp_query;
		        if ($wp_query->post_count == 1) {
		            wp_redirect( get_permalink( $wp_query->posts['0']->ID ) );
		        }
		    }
		}
	}

	// remove rss version
	if( ! function_exists( 'wdf_rss_version ' ) ) {
		function wdf_rss_version() {
			return '';
		}
	}

	// add html tags to editor
	if( ! function_exists( 'fb_change_mce_options' ) ) {
		function fb_change_mce_options($initArray) {
		    // comma separated string od extendes tags
		    // command separated string of extended elements
		    $ext = 'pre[id|name|class|style],iframe[align|longdesc|name|width|height|frameborder|scrolling|marginheight|marginwidth|src]';
		    if ( isset( $initArray['extended_valid_elements'] ) ) {
		    $initArray['extended_valid_elements'] .= ',' . $ext;
		    } else {
		        $initArray['extended_valid_elements'] = $ext;
		    }
		    // maybe; set tiny paramter verify_html
		    // $initArray['verify_html'] = false;
		    return $initArray;
		}
	}

	// edit search for permalinks
	if( ! function_exists( 'change_search_url_rewrite' ) ) {
	    function change_search_url_rewrite() {
	        if ( is_search() && ! empty( $_GET['s'] ) ) {
	            wp_redirect( home_url( "/search/" ) . urlencode( get_query_var( 's' ) ) );
	            exit();
	        }
	    }
	}

	// add ie conditional wrapper
	if( ! function_exists( 'wdf_ie_conditional ' ) ) {
		function wdf_ie_conditional( $tag, $handle ) {
			if ( 'source-ie-only' == $handle )
				$tag = '<!--[if lt IE 9]>' . "\n" . $tag . '<![endif]-->' . "\n";
			return $tag;
		}
	}

	// add subdomain redirects
	if( ! function_exists( 'allowing_redirect_hosts' ) ) {
	    function allowing_redirect_hosts( $content ) {
	       // $content[] = 'dev.website.com';
	       // $content[] = 'prod.website.com';
	       // $content[] = 'forum.website.com';
	        return $content;
	    }
	}

	add_action( 'after_setup_theme', 'wdf_clean', 999 );

	// fix admin bar margin
	function admin_bar_fix() {
		if ( is_admin_bar_showing() ) {
			?>
			<style type="text/css" media="screen">
				html { margin-top: 32px !important; }
				* html body { margin-top: 32px !important; }
			</style>
			<?
		} else {
			// do nothing
		}
	}

	add_action( 'wp_head', 'admin_bar_fix', 11 );

?>
