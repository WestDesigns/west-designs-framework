<?php

	// page.php
	// description: default page.
	// ----------------------------------------------------------------

get_header(); ?>

<div role="page" class="global">
	<main>
		<article>
			<?php // page loop
				if ( have_posts() ) : while ( have_posts() ) : the_post();
					the_content();
				endwhile;
				else :
                    include_once('assets/parts/missing.php');
				endif;
			?>
		</article>
		<aside>
			<?php get_sidebar(); ?>
		</aside>
	</main>
</div>

<?php get_footer(); ?>
