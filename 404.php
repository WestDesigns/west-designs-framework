<?php

	// 404.php
	// description: page for 404 redirects.
	// ----------------------------------------------------------------

get_header(); ?>

<div role="404">
	<main>
		<section>
			<img src="<?php bloginfo('template_directory'); ?>/assets/images/404.png" alt="404" />
            <p>
                This page no longer exists or has been moved to a new location. We apologize for your inconvenience.
            </p>
            <a href="<?php echo get_home_url(); ?>" class="button">Back To Home</a>
		</section>
	</main>
</div>

<?php get_footer(); ?>
