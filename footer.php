<?php

	// footer.php
	// description: page for the footer.
	// ----------------------------------------------------------------

	get_header();

	// edit-this-link
	$current_user = wp_get_current_user();
	if ( user_can($current_user, 'administrator') ) {
		?>
			<div role="edit-this-entry">
				<a href="<?php echo get_edit_post_link(); ?>" class="button">
					<i class="fa fa-paper-plane"></i>
					Edit This Entry
				</a>
			</div>
		<?
	} else {
		// do nothing
	};

?>

<div role="links">
	<section>
		<?php wdf_navigation_footer(); ?>
	</section>
</div>

<div role="widgets">
    <section>
        <?php
            // left
			if ( is_active_sidebar('left-widget') ) :
				dynamic_sidebar('left-widget');
			else :
				// do nothing
			endif;

            // center
            if ( is_active_sidebar('center-widget') ) :
				dynamic_sidebar('center-widget');
			else :
				// do nothing
			endif;

            // right
            if ( is_active_sidebar('right-widget') ) :
				dynamic_sidebar('right-widget');
			else :
				// do nothing
			endif;
		?>
    </section>
</div>

<footer>
	<section>
		<p>
			&copy; <?php echo date('Y'); ?> by <?php bloginfo('name'); ?>.
			<a href="http://designsbywest.com" target="_blank" title="Web Design">Web Design</a> by <a href="http://designsbywest.com" target="_blank" title="West Designs">West Designs</a>
		</p>
	</section>
</footer>

<!-- google analytics -->
<?php // TODO: add google analytics ?>

<?php wp_footer(); ?>

</body>
</html>
